package question1;

public class Date {
	private int month;
	private int day;
	private int year;

	public Date() {
		this.month = 1;
		this.day = 1;
		this.year = 1900;
	}

	public Date(int day,int month, int year) {
		this.day = day;
		this.month = month;
		this.year = year;
	}

	public int getMonth() {
		return month;
	}

	public void setMonth(int month) {
		if(month>=1 && month <=12) {
			this.month = month;
		}
	}

	public int getDay() {
		return day;
	}

	public void setDay(int day) {
		int numOfDays = this.numberOfDays();
		if(day <= numOfDays) {
			this.day = day;
		}
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		if(year<2018 && year>0) {
			this.year = year;
		}
	}

	@Override
	public String toString() {
		return day + "-" + month + "-" + year;
	}

	public static boolean isLeap(int year) {
		if ((year % 4 == 0) && year % 100 != 0)
		{
			return true;
		}
		else if ((year % 4 == 0) && (year % 100 == 0) && (year % 400 == 0))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	public int numberOfDays() {
		int month = this.month;
		if((month < 8 && month % 2 == 1) || (month > 7 && month % 2 == 0)) {
			return 31;
		}else {
			if(month != 2) {
				return 30;
			}else {
				if(month == 2 && isLeap(this.year)) {
					return 29;
				}else {
					return 28;
				}
			}
		}

	}
	public int daysPassed() {
		if(this.month == 1) {
			return this.day;
		}
		int fullMonthPassed = this.month -1;
		int count = 0;
		for(int i = 1; i <= fullMonthPassed; i++) {
			Date newDate = new Date(1,i,1900);
			count += newDate.numberOfDays();
		}
		return count + this.day;

	}
	public int daysRemaining() {
		if(isLeap(this.year)) {
			return 366 - this.daysPassed();
		}
		return 365 - this.daysPassed();
	}
	public Date calculateNewDate(int days) {
		Date result = new Date();
		result.setDay(this.day);
		result.setMonth(this.month);
		result.setYear(this.year);
		do {
			if(days <= result.numberOfDays()-result.day) {
				result.setDay(result.day + days);
				return result;
			}else {
				if(result.month < 12) {
					days -= (result.numberOfDays()-result.day + 1);
					System.out.println("broja dana je " + days);
					result.setDay(1);
					result.setMonth(result.month + 1);
				}else {
					days -= (result.numberOfDays()-result.day + 1);
					result.setDay(1);
					result.setMonth(1);
					if(result.year + 1 > 2017) {
						System.out.println("Number of years passed 2018");
						return new Date();
					}else {
						result.setYear(result.year + 1);
					}
				}
			}
		}while(days > 0);
		return result;
	}
	@Override
	public boolean equals(Object dateObj) {
		Date date = (Date)dateObj;
		return this.day == date.day && this.month == date.month && this.year == date.year;
	}
	public static String age(Date birth, Date current) {
		int ageDay = 0;
		int ageMonth = 0;
		int ageYear = 0;
		int currentMonth = current.month;
		int currentYear = current.year;
		if(current.day>birth.day) {
			ageDay = current.day - birth.day;
		}else {
			ageDay = current.day + current.numberOfDays() - birth.day;
			currentMonth --;
		}
		if(currentMonth > birth.month) {
			ageMonth = currentMonth - birth.month;
		}else {
			ageMonth = currentMonth + 12 - birth.month;
			currentYear --;
		}
		ageYear = currentYear - birth.year;
		return "Age is " + ageDay + " days " + ageMonth + " months " + ageYear + " years." ;
	}
}
