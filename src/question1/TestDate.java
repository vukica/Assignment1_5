package question1;
import question1.Date;

public class TestDate {
	
	public static void main(String args[]) {
		//1. testing is leap year
		System.out.println(1988 + " leap is " + Date.isLeap(1988));
		//2. number of days in month
		Date date = new Date(22,3,2011);
		System.out.println("Number of days in month for date " + date + " is " + date.numberOfDays());
		// 3. number of passed days
		Date date1 = new Date(18,12,2013);
		System.out.println(date1);
		System.out.println("Days passed " + date1.daysPassed());
		//4. number of days remaining
		System.out.println("Number of days remaining " + date1.daysRemaining());
		//5. calculating the new date
		System.out.println( date1.calculateNewDate(70));
		//6. comparing dates
		System.out.println(date + " & " + date1 + " are equals " + date.equals(date1));
		//7. calculating age
		System.out.println(Date.age(date, date1));
		
	}
}
